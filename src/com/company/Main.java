package com.company;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Object object = new Object();
        Apple apple = new Apple();
        Fruit fruit = new Fruit();
        Orange orange = new Orange();
        Citrus citrus =new Citrus();
        Squeezable squeezable;

        //**casting de Fruit a Squeezable
        try {
            Apple apple1=(Apple)fruit;
            System.out.println("cheking cast for FruitApple: yes casting ready ");
        } catch (ClassCastException e) {

            System.out.println("No casting ready: " + e.getMessage());
       }
        //**casting de Fruit a Citrus
        try{
            Citrus citrus1=(Citrus)fruit;
            System.out.println("cheking cast for FruitCitrus: yes casting ready ");
        } catch (ClassCastException e) {

            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Fruit a Squeezable
        try {
            Squeezable squeezable1 = (Squeezable) fruit;
            System.out.println("cheking cast for FruitSqeezable:yes casting ready");

        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Fruit a Orange
        try {
            Orange orange1=(Orange)fruit;
            System.out.println("cheking cast for FruitOrage:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Apple a Sqeezable
        try {
            Squeezable squeezable1 = (Squeezable) apple;
            System.out.println("cheking cast for AppleSqeezable:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Apple a Fruit
        try {
           Fruit fruit1=(Fruit)apple;
            System.out.println("cheking cast for AppleFruit:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Citrus a Fruit
        try {
            Fruit fruit1=(Fruit)citrus;
            System.out.println("cheking cast for CitrusFruit:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Citrus a Orange
        try {
            Orange orange1=(Orange)citrus;
            System.out.println("cheking cast for CitrusOrange:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Citrus a squeezable
        try {
            Squeezable squeezable1=(Squeezable) citrus;
            System.out.println("cheking cast for CitrusSqueezable:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Orange a Fruit
        try {
            Fruit fruit1=(Fruit)orange;
            System.out.println("cheking cast for OrangeFruit:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Orange a squeezable
        try {
            Squeezable squeezable1=(Squeezable) orange;
            System.out.println("cheking cast for OrangeSqueezable:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }
        //**casting de Orange a Citrus
        try {
            Citrus citrus1=(Citrus) orange;
            System.out.println("cheking cast for OrangeCitrus:yes casting ready ");
        } catch (ClassCastException e) {
            System.out.println("No casting ready: " + e.getMessage());
        }

    }
}
